import java.util.Random;

class Sort {

    private static int MAX_LENGTH = 11;

    public static void main(String[] args) {
        int array[] = new int[MAX_LENGTH];
        generateAnArray(array);
        bubbleSort(array);
    }

    private static void bubbleSort(int array[]) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    //swap elements
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }

        System.out.print("\nAfter sorting: ");
        printArray(array);
    }


    /*
        Improved bubble sorting using swap flag (boolean) that will reduce the number of iterations,
        as once the array is sorted we will break.
     */
    public static void bubbleSortImproved(int array[]) {
        for (int i = 0; i < array.length - 1; i++) {
            boolean swap = false;
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    swap = true;

                    //swap elements
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }

            if (!swap) {
                break;
            }
        }

        System.out.print("After sorting: ");
        printArray(array);
    }

    /*
        Method for generating elements of the array from 0 to 20
     */
    private static void generateAnArray(int array[]) {

        Random random = new Random();

        for (int i = 0; i < MAX_LENGTH; i++) {
            array[i] = random.nextInt(20)+1;
            System.out.print(array[i] + " ");
        }
    }

    /*
       Method to print the array
    */
    private static void printArray(int array[]) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();
    }
}